<?php


namespace app\models\History;

use app\models\Call as CallModel;

/**
 * Class Call
 * @package app\models\History
 *
 * @property bool $isAnswered
 * @property CallModel $call
 */
class Call extends Event
{
    /**
     * @inheritDoc
     */
    public function getBodyText()
    {
        // HTML Теги в моделях - плохо
        if ($this->call) {
            return $this->call->totalStatusText . ($this->call->getTotalDisposition(false) ? " <span class='text-grey'>" . $this->call->getTotalDisposition(false) . "</span>" : "");
        } else {
            return '<i>' . \Yii::t('app', 'Deleted') . '</i>';
        }
    }

    /**
     * Пример метода, который удобно можно описывать в модели, не дублируя логику в представлениях
     *
     * @return bool
     */
    public function getIsAnswered()
    {
        return $this->call && $this->call->status == CallModel::STATUS_ANSWERED;
    }
}