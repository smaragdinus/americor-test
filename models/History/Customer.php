<?php


namespace app\models\History;

use app\models\Customer as CustomerModel;

/**
 * Class Customer
 * @package app\models\History
 *
 * @property string $oldValue
 * @property string $newValue
 * @property CustomerModel $customer
 */
class Customer extends Event
{
    public function getBodyText()
    {
        return $this->eventText . ' ' .
            ($this->oldValue ?? "not set") . ' to ' .
            ($this->newValue ?? "not set");
    }

    public function getOldValue()
    {
        return '';
    }

    public function getNewValue()
    {
        return '';
    }

}