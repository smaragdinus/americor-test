<?php


namespace app\models\History;

use app\models\Customer as CustomerModel;

/**
 * Class CustomerQuality
 * @package app\models\History
 */
class CustomerQuality extends Customer
{
    public function getOldValue()
    {
        return CustomerModel::getQualityTextByQuality($this->getDetailOldValue('quality'));
    }

    public function getNewValue()
    {
        return CustomerModel::getQualityTextByQuality($this->getDetailNewValue('quality'));
    }
}