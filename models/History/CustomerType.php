<?php


namespace app\models\History;

use app\models\Customer as CustomerModel;

/**
 * Class CustomerType
 * @package app\models\History
 */
class CustomerType extends Customer
{
    public function getOldValue()
    {
        return CustomerModel::getTypeTextByType($this->getDetailOldValue('type'));
    }

    public function getNewValue()
    {
        return CustomerModel::getTypeTextByType($this->getDetailNewValue('type'));
    }
}