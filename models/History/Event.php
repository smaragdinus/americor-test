<?php


namespace app\models\History;


use app\models\History;
use yii\base\InvalidConfigException;

/**
 *
 * @property void $date
 * @property void $bodyText
 * @property void $typeName
 * @property void $userName
 */
class Event extends History
{
    const TYPE = 'default';

    // Информацию о связи имени события и класса модели можно убрать в любое место. конфиги, хранилища и т.п.
    protected static $_types = [
        Task::class => [
            self::EVENT_COMPLETED_TASK,
            self::EVENT_UPDATED_TASK,
            self::EVENT_CREATED_TASK,
        ],
        CustomerQuality::class => [
            self::EVENT_CUSTOMER_CHANGE_QUALITY,
        ],
        CustomerType::class => [
            self::EVENT_CUSTOMER_CHANGE_TYPE,
        ],
        Call::class => [
            self::EVENT_OUTGOING_CALL,
            self::EVENT_INCOMING_CALL,
        ],
        Fax::class => [
            self::EVENT_OUTGOING_FAX,
            self::EVENT_INCOMING_FAX,
        ],
        Sms::class => [
            self::EVENT_OUTGOING_SMS,
            self::EVENT_INCOMING_SMS,
        ],
    ];

    protected static $_classes = null;

    /**
     * @inheritDoc
     *
     * @param array $row
     * @return Event
     */
    public static function instantiate($row)
    {
        $class = self::getClassByEvent($row['event']);

        return new $class;
    }

    /**
     * @inheritDoc
     */
    public static function find()
    {
        $query = parent::find();
        $events = self::getEventsByClass(get_called_class());
        if (!empty($events)) {
            $query->andWhere(['event' => $events]);
        }

        return $query;
    }

    /**
     * Возвращает список типов событий для заданного класса
     *
     * @param string $class Имя класса
     * @return string[] Типы событий
     */
    public static function getEventsByClass($class)
    {
        return self::$_types[$class] ?? [];
    }

    /**
     * Возвращает имя класса соответсвующего событию
     *
     * @param string $eventName имя события
     * @return string имя класса
     */
    public static function getClassByEvent($eventName)
    {
        if (is_null(self::$_classes)) {
            self::$_classes = [];
            foreach (self::$_types as $class => $events) {
                foreach ($events as $event) {
                    self::$_classes[$event] = $class;
                }
            }
        }

        return self::$_classes[$eventName] ?? self::class;
    }

    /**
     * @return string Форматированная дата события
     * @throws InvalidConfigException
     */
    public function getDate()
    {
        return \Yii::$app->formatter->asDatetime($this->ins_ts);
    }

    /**
     * @return string Имя пользователя, инициировавшего событие
     */
    public function getUserName()
    {
        return $this->user ? $this->user->username : \Yii::t('app', 'System');
    }

    /**
     * @return string Название типа объекта, с которым связано событие
     */
    public function getTypeName()
    {
        return $this->object;
    }

    /**
     * @inheritDoc
     */
    public function getEventText()
    {
        return parent::getEventText();
    }

    /**
     * @return string Текст, описывающий событие
     */
    public function getBodyText()
    {
        return $this->eventText;
    }
}