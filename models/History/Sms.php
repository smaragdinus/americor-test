<?php


namespace app\models\History;

/**
 * Class Sms
 * @package app\models\History
 *
 * @property \app\models\Sms $sms
 */
class Sms extends Event
{
    public function getBodyText()
    {
        return $this->sms->message ?? '';
    }
}