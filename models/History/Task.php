<?php


namespace app\models\History;

/**
 * Class Task
 * @package app\models\History
 *
 * @property \app\models\Task $task
 */
class Task extends Event
{
    public static function getEventTypes()
    {
        return [
            self::EVENT_COMPLETED_TASK,
            self::EVENT_UPDATED_TASK,
            self::EVENT_CREATED_TASK
        ];
    }

    public function getBodyText()
    {
        return $this->eventText . ": " . ($this->task->title ?? '');
    }
}