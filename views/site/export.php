<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $exportType string
 */

use app\models\History;
use app\widgets\Export\Export;
use app\widgets\HistoryList\helpers\HistoryListHelper;

$filename = 'history';
$filename .= '-' . time();

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
?>

<?= Export::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'date',
            'label' => Yii::t('app', 'Date'),
        ],
        [
            'attribute' => 'userName',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'typeName',
            'label' => Yii::t('app', 'Type'),
        ],
        [
            'attribute' => 'eventText',
            'label' => Yii::t('app', 'Event'),
        ],
        [
            'attribute' => 'bodyText',
            'label' => Yii::t('app', 'Message'),
        ]
    ],
    'exportType' => $exportType,
    'batchSize' => 2000,
    'filename' => $filename
]);