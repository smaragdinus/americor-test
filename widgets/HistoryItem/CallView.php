<?php


namespace app\widgets\HistoryItem;


use app\models\History\Call;
use app\models\Call as CallModel;

/**
 * Class CallView
 * @package app\widgets\HistoryItem
 *
 * @property Call $model
 */
class CallView extends HistoryItem
{
    public function run()
    {
        /** @var CallModel $call */
        $call = $this->model->call;
        return $this->render($this->viewName, [
            'user' => $this->model->user,
            'content' => $call->comment ?? '',
            'body' => $this->model->bodyText,
            'footerDatetime' => $this->model->ins_ts,
            'footer' => isset($call->applicant) ? "Called <span>{$call->applicant->name}</span>" : null,
            'iconClass' => $this->model->isAnswered ? 'md-phone bg-green' : 'md-phone-missed bg-red',
            'iconIncome' => $this->model->isAnswered && $call->direction == CallModel::DIRECTION_INCOMING
        ]);
    }
}