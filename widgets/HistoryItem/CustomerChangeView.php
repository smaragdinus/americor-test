<?php


namespace app\widgets\HistoryItem;


use app\models\History\Customer;

/**
 * Class CustomerChangeView
 * @package app\widgets\HistoryItem
 *
 * @property Customer $model
 */
class CustomerChangeView extends HistoryItem
{
    public $viewName = 'change';

    public function run()
    {
        return $this->render($this->viewName, [
            'model' => $this->model,
            'oldValue' => $this->model->oldValue,
            'newValue' => $this->model->newValue,
        ]);
    }
}