<?php


namespace app\widgets\HistoryItem;


use app\models\History\Fax;

/**
 * Class FaxView
 * @package app\widgets\HistoryItem
 *
 * @property Fax $model
 */
class FaxView extends HistoryItem
{
    public function run()
    {
        $fax = $this->model->fax;

        return $this->render($this->viewName, [
            'user' => $this->model->user,
            'body' => $this->model->bodyText .
                ' - ' .
                (isset($fax->document) ? \yii\helpers\Html::a(
                    \Yii::t('app', 'view document'),
                    $fax->document->getViewUrl(),
                    [
                        'target' => '_blank',
                        'data-pjax' => 0
                    ]
                ) : ''),
            'footer' => \Yii::t('app', '{type} was sent to {group}', [
                'type' => $fax ? $fax->getTypeText() : 'Fax',
                'group' => isset($fax->creditorGroup) ? \yii\helpers\Html::a($fax->creditorGroup->name, ['creditors/groups'], ['data-pjax' => 0]) : ''
            ]),
            'footerDatetime' => $this->model->ins_ts,
            'iconClass' => 'fa-fax bg-green'
        ]);
    }
}