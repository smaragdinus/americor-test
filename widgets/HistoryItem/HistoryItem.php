<?php


namespace app\widgets\HistoryItem;


use app\models\History\Call;
use app\models\History\Customer;
use app\models\History\Event;
use app\models\History\Fax;
use app\models\History\Sms;
use app\models\History\Task;
use yii\base\Widget;

/**
 *
 * @property array $rules
 */
class HistoryItem extends Widget
{
    /**
     * @var Event
     */
    public $model;

    /**
     * Упорядоченный список правил применения виджета
     * В качестве ключа указывается класс виджета,
     * а в качестве элемента может выступать callable
     * принимающий аргумент $config - конфиг виджета,
     * так же для сокращения описания можно передавать
     * имя класса, на который будет проверяться $config['model']
     *
     * В случае, если проверка вернула true - выполняется виджет,
     * класс которого указан в качестве ключа.
     *
     * В случае, когда ни одно из правил не подошло - будет
     * запущен виджет HistoryItem
     * ```php
     * [
     *  TaskView::class => function ($config) {
     *      return $config['model'] instanceof Task;
     *  },
     *  SmsView::class => Sms::class,
     *  FaxView::class => Fax::class,
     *  CustomerChangeView::class => Customer::class,
     *  CallView::class => Call::class,
     * ];
     * ```
     * @var array
     */
    public $rules = [];

    /**
     * @var string имя файла представления
     */
    public $viewName = 'default';

    protected static $_rules = null;

    /**
     * Список правил применения виджетов по умолчанию
     * @return array
     * @see HistoryItem::$rules
     */
    public static function getRules()
    {
        /*
         * Так же можно подумать над структурой правил,
         * что бы можно было передавать все настройки виджета, а не только его класс
         */
        if (is_null(self::$_rules)) {
            self::$_rules = [
                TaskView::class => function ($config) {
                    return $config['model'] instanceof Task;
                },
                SmsView::class => Sms::class,
                FaxView::class => Fax::class,
                CustomerChangeView::class => Customer::class,
                CallView::class => Call::class,
            ];
        }

        return self::$_rules;
    }

    /**
     * Проверка $config на соответствие правилам HistoryItem::$rules
     * И запуск соответствующего виджета.
     * Возвращается результат запуска виджета.
     * @param array $config
     * @return string
     * @throws \Exception
     */
    public static function runWidget($config = [])
    {
        $rules = $config['rules'] ?? self::getRules();
        foreach ($rules as $widget => $rule) {
            $resolve = false;
            if (is_callable($rule)) {
                $resolve = call_user_func($rule, $config);
            } elseif (is_string($rule)) {
                $resolve = is_a($config['model'], $rule);
            }

            if ($resolve) {
                /** @var HistoryItem $widget */
                return $widget::widget($config);
            }
        }

        return self::widget($config);
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        return $this->render($this->viewName, [
            'model' => $this->model,
            'user' => $this->model->user,
            'body' => $this->model->bodyText,
            'bodyDatetime' => $this->model->ins_ts,
            'iconClass' => 'fa-gear bg-purple-light'
        ]);
    }

}