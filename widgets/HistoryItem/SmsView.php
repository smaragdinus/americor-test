<?php


namespace app\widgets\HistoryItem;


use app\models\History\Sms;
use app\models\Sms as SmsModel;

/**
 * Class SmsView
 * @package app\widgets\HistoryItem
 *
 * @property Sms $model
 */
class SmsView extends HistoryItem
{
    public function run()
    {
        return $this->render($this->viewName, [
            'user' => $this->model->user,
            'body' => $this->model->bodyText,
            'footer' => $this->model->sms->direction == SmsModel::DIRECTION_INCOMING ?
                \Yii::t('app', 'Incoming message from {number}', [
                    'number' => $model->sms->phone_from ?? ''
                ]) : \Yii::t('app', 'Sent message to {number}', [
                    'number' => $model->sms->phone_to ?? ''
                ]),
            'iconIncome' => $this->model->sms->direction == SmsModel::DIRECTION_INCOMING,
            'footerDatetime' => $this->model->ins_ts,
            'iconClass' => 'icon-sms bg-dark-blue'
        ]);
    }
}