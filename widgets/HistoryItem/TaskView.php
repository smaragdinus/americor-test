<?php


namespace app\widgets\HistoryItem;


use app\models\History\Task;

/**
 * Class TaskView
 * @package app\widgets\HistoryItem
 *
 * @property Task $model
 */
class TaskView extends HistoryItem
{
    public function run()
    {
        return $this->render($this->viewName, [
            'user' => $this->model->user,
            'body' => $this->model->bodyText,
            'iconClass' => 'fa-check-square bg-yellow',
            'footerDatetime' => $this->model->ins_ts,
            'footer' => isset($this->model->task->customerCreditor->name) ? "Creditor: " . $this->model->task->customerCreditor->name : ''
        ]);
    }
}