<?php

use app\models\search\HistorySearch;
use app\widgets\HistoryItem\HistoryItem;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */
/* @var $model HistorySearch */
/* @var $linkExport string */

?>

<?php Pjax::begin(['id' => 'grid-pjax', 'formSelector' => false]); ?>

<div class="panel panel-primary panel-small m-b-0">
    <div class="panel-body panel-body-selected">

        <div class="pull-sm-right">
            <?php if (!empty($linkExport)) {
                echo Html::a(Yii::t('app', 'CSV'), $linkExport,
                    [
                        'class' => 'btn btn-success',
                        'data-pjax' => 0
                    ]
                );
            } ?>
        </div>

    </div>
</div>

<?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model, $key, $index, $widget) {
        // Запустим фабрику виджетов, она сама разберётся, какой виджет запустить
        return HistoryItem::runWidget(['model' => $model]);
        /*
         * Так же можно передавать в фабрику свои правила подстановки виджетов
         * HistoryItem::runWidget([
         *   'model' => $model,
         *   'rules' => [
         *       AnyTaskView::class => Task::class,
         *       AnySmsView::class => Sms::class,
         *       AnyFaxView::class => Fax::class,
         *       AnyCustomerChangeView::class => Customer::class,
         *       AnyCallView::class => Call::class,
         *   ]
         * ]);
         */
    },
    'options' => [
        'tag' => 'ul',
        'class' => 'list-group'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'list-group-item'
    ],
    'emptyTextOptions' => ['class' => 'empty p-20'],
    'layout' => '{items}{pager}',
]); ?>

<?php Pjax::end(); ?>
